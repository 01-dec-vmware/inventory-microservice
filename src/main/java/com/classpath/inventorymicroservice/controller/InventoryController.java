package com.classpath.inventorymicroservice.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/inventory")
@RequiredArgsConstructor
@Slf4j
public class InventoryController {

    private static int counter = 1000;

    private final Environment environment;

    @GetMapping
    public int getInventory(){
        return counter;
    }

    @PostMapping
    public int updateQty(){
        log.info("Inside the Inventory microservice :: {}" ,environment.getProperty("server.host"));
        return --counter;
    }
}